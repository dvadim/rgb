package test;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Assert;
import org.junit.Test;

import rgb.Color;
import rgb.Main;

public class MainTest {

	@Test
	public void testGetAllColorsUniqueÑolor() {
		List<Color> listColors = new Main().getAllColors();
		int sizeBefore = listColors.size();
		Set<Color> setColors = new HashSet<Color>(listColors);
		Assert.assertTrue(sizeBefore == setColors.size());
	}

	@Test
	public void testGetAllColorsMultiple() {
		int multiple = Main.FOLD;
		List<Color> colors = new Main().getAllColors();
		for (Color color : colors) {
			Assert.assertTrue("Red color don't multiple " + multiple, color.getRed() % multiple == 0);
			Assert.assertTrue("Green color don't multiple " + multiple, color.getGreen() % multiple == 0);
			Assert.assertTrue("Blue color don't multiple " + multiple, color.getBlue() % multiple == 0);
		}
	}

	@Test
	public void testGetAllColorsRange() {
		int min = Main.FOLD;
		int max = 255;
		List<Color> colors = new Main().getAllColors();
		for (Color color : colors) {
			Assert.assertFalse("An incorrect range of value red color", color.getRed() < min || color.getRed() > max);
			Assert.assertFalse("An incorrect range of value green color",
					color.getGreen() < min || color.getGreen() > max);
			Assert.assertFalse("An incorrect range of value blue color",
					color.getBlue() < min || color.getBlue() > max);
		}
	}

	@Test
	public void testGetAllColorsSize() {
		int iterations = 255 / Main.FOLD;
		int amountRGBColors = 3;
		Main app = new Main();
		Assert.assertTrue(Math.pow(iterations, amountRGBColors) == app.getAllColors().size());
	}

}
