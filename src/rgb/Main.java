package rgb;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class Main {

	public static final int FOLD = 51;

	public static final int NUMBER_FIRST_ELEMENTS = 9;

	public static void main(String[] args) {

		JFrame frame = new JFrame("RGB");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(800, 600);
		frame.setLocationRelativeTo(null);

		JPanel panelRoot = new JPanel();
		panelRoot.setLayout(new BoxLayout(panelRoot, BoxLayout.Y_AXIS));
		frame.add(panelRoot);

		Main app = new Main();
		List<Color> colors = app.getAllColors();
		app.sortColors(colors);
		int i = 1;
		for (Color color : colors) {
			if (i++ <= NUMBER_FIRST_ELEMENTS) {
				JPanel panelColor = new JPanel();
				panelColor.setBackground(new java.awt.Color(color.getRed(), color.getGreen(), color.getBlue()));
				panelRoot.add(panelColor);
			}

			System.out.println(color);
		}

		frame.setVisible(true);

	}

	private void sortColors(List<Color> colors) {
		Collections.shuffle(colors);
		Collections.sort(colors, (Color c1, Color c2) -> c1.getRed() - c2.getRed() + c2.getBlue() - c1.getBlue());
	}

	public List<Color> getAllColors() {
		int iteration = 255 / FOLD;
		List<Color> colors = new ArrayList<>();
		for (int r = 1; r <= iteration; r++) {
			for (int b = iteration; b >= 1; b--) {
				for (int g = 1; g <= iteration; g++) {
					colors.add(new Color(FOLD * r, FOLD * g, FOLD * b));
				}
			}
		}

		return colors;
	}

}
